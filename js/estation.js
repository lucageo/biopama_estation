(function ($, Drupal) {
    var eStationCards = drupalSettings.eStationCards;

    $('.estation-data-card').on('shown.bs.collapse', function () {

        var selectedTopic = $(this).closest( ".card-body" ).find(".inner-card-title").text().trim();
        var selectedCategory = $(this).closest( ".wrapper-card" ).prev().text().trim();
        var topicID = "#collapse" + selectedTopic;
        console.log(selectedTopic)

        if ($("#eStation-"+selectedTopic+"-subproducts").length > 0){
            var selectedSubProductName = $( "#eStation-"+selectedTopic+"-subproducts" ).find(":selected").text();
            eStationMap(selectedTopic, selectedSubProductName);
            return;
        }

        var eStationURL = "https://estation.jrc.ec.europa.eu/eStation2/analysis/gettimeseriesprofile";

        if ($(topicID +" #eStation-subproducts").length > 0){
            $(topicID +" #eStation-subproducts").remove();
        }
        if ($(topicID +" #eStation-years").length > 0){
            $(topicID +" #eStation-years").remove();
        }
        if ($(topicID +" .estation-data-source").length > 0){
            $(topicID +" .estation-data-source").remove();
        }
        var subproductsSelectList = document.createElement("select");
        subproductsSelectList.id = "eStation-"+selectedTopic+"-subproducts";
        $(this).find(".estation-results").append( $( subproductsSelectList ) );
        $( subproductsSelectList ).addClass("form-control");
        var option = document.createElement("option");
        option.value = null;
        option.text = "Select a product";
        subproductsSelectList.appendChild(option);


        for (var i = 0; i < eStationCards[selectedCategory].subproducts.length; i++) {
            $("#eStation-"+selectedTopic+"-subproducts").append('<option value="'+eStationCards[selectedCategory].subproducts[i].code + '">' + eStationCards[selectedCategory].subproducts[i].name + '</option>'); 
        }

        $( "#eStation-"+selectedTopic+"-subproducts" ).change(function() {
            if ($("#eStation-"+selectedTopic+"-years").length > 0){
                $("#eStation-"+selectedTopic+"-years").remove();
            }

            var selectedSubProductName = $( "#eStation-"+selectedTopic+"-subproducts" ).find(":selected").text();
            var selectedSubProductVal = $( "#eStation-"+selectedTopic+"-subproducts" ).find(":selected").val();

            var prodList = eStationCards[selectedCategory].subproducts
            var thisProd = prodList.filter(obj => {
                return obj.name === selectedSubProductName
            });
            console.log(thisProd)

            if ($('#estation-'+selectedTopic+'-data-source').length > 0){
                $('#estation-'+selectedTopic+'-data-source').remove();
            }

            var infoIcon = '<a href="'+ ( thisProd[0].hasOwnProperty('link') ? thisProd[0].link : eStationCards[selectedCategory].link ) +'" target="_blank" id="estation-'+selectedTopic+'-data-source" class="estation-data-source btn btn-sm btn-secondary p-0" style="--bs-btn-border-radius: 50%;" data-bs-toggle="tooltip" data-bs-html="true" data-bs-placement="right" aria-label="Data Source: ' + ( thisProd[0].hasOwnProperty('provider') ? thisProd[0].provider : eStationCards[selectedCategory].provider ) +'" data-bs-original-title="Data Source: ' + ( thisProd[0].hasOwnProperty('provider') ? thisProd[0].provider : eStationCards[selectedCategory].provider ) +'">'+
                        '<i class="fa-solid fa-database"></i>'+
                    '</a>';
            $(topicID).closest(".card-body").find(".info-icon").append( $( infoIcon ) );

            var dataSource = document.getElementById('estation-'+selectedTopic+'-data-source')
            bootstrap.Tooltip.getOrCreateInstance(dataSource);

            var description = thisProd[0].description;
            $(topicID).find("#estation-description").empty().append( description );
            

            eStationMap(selectedTopic, selectedSubProductName);
            
            var yearsSelectList = document.createElement("select");
            yearsSelectList.id = "eStation-"+selectedTopic+"-years";
            $(topicID).find(".estation-results").append( $( yearsSelectList ) );
            $( yearsSelectList ).addClass("form-control w-25 estation-years");
            //var selectedTopic = $( "#eStation-Topics" ).find(":selected").val();
            //var years = eStationCards[selectedTopic].years;
            //years.reverse(); // so the most recent year appears first
            
            for (var i = 1; i < 20; i++) {
                var thisYear = moment().startOf('year').subtract(i, 'years').format('YYYY')
                $("#eStation-"+selectedTopic+"-years").append('<option value="' + thisYear + '">' + thisYear + '</option>'); 
            }


            $("#eStation-"+selectedTopic+"-years").change(function() {
                $().insertBiopamaLoader("#estation-"+selectedTopic+"-chart"); 

                var selectedYear = $( "#eStation-"+selectedTopic+"-years" ).find(":selected").val();
                var xhr = new XMLHttpRequest();
                xhr.open("POST", eStationURL);

                xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

                xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    //console.log(xhr.status);
                    //console.log(xhr.responseText);
                    eStationChart(selectedTopic, selectedSubProductName, xhr.responseText)
                }};

                var data = eStationData(selectedTopic, selectedSubProductVal, selectedYear);
                xhr.send(data);
                
            });
            $("#eStation-"+selectedTopic+"-years").trigger('change');
        });

        function eStationData(topic, product, year){

            var prodList = eStationCards[selectedCategory].subproducts
            var thisProd = prodList.filter(obj => {
                return obj.code === product
            })

            var data = "productcode=" + ( thisProd[0].hasOwnProperty('productcode') ? thisProd[0].productcode : eStationCards[selectedCategory].productcode ) +
            "&version=" + ( thisProd[0].hasOwnProperty('version') ? thisProd[0].version : eStationCards[selectedCategory].version ) +
            "&subproductcode="+product+
            "&mapsetcode=" + ( thisProd[0].hasOwnProperty('mapsetcode') ? thisProd[0].mapsetcode : eStationCards[selectedCategory].mapsetcode ) +
            "&yearsToCompare=%5B"+year+"%5D"+
            "&WKT="+thisWKT;
            console.log(data)
            return data;
        }

        function eStationChart(topic, product, response){
            $().removeBiopamaLoader("#estation-"+topic+"-chart"); 
            var eStationData = JSON.parse(response);
            var timeSeries = eStationData.timeseriesvalue;
            var dates = [];
            var values = [];
            timeSeries.forEach(function (arrayItem) {
                dates.push(moment(arrayItem.date, 'YYYYMMDD').format('DD/MM/YYYY'));
                values.push(arrayItem.value);
            });
            var prodList = eStationCards[selectedCategory].subproducts
            var thisProd = prodList.filter(obj => {
                return obj.name === product
            })
            
            var chartData = {
                title: product + " for " +  selSettings.paName,
                yAxis: [
                    {
                        position: "right",
                        alignTicks: true,
                        nameLocation: 'middle',
                        nameGap: 70,
                        axisLabel: {
                            formatter: '{value}',
                            color: biopamaGlobal.chart.colors.text
                        }
                    }
                ],
                xAxis: [
                    {
                        type: 'category',
                        data: dates
                    }
                ],
                series: [
                    {
                        name: 'yyy',
                        type: thisProd[0].chartType,
                        data: values
                    },
                ]
            }
            $().createXYAxisChart("estation-"+topic+"-chart" , chartData);  
            
        }

        function eStationMap(topic, product){
            var prodList = eStationCards[selectedCategory].subproducts
            
            var thisProd = prodList.filter(obj => {
                return obj.name === product
            })

            if (mymap.getLayer("eStation-layer")) {
                mymap.removeLayer("eStation-layer");
            }
            if (mymap.getSource("eStation-source")) {
                mymap.removeSource("eStation-source");
            }

            if (thisProd[0].mapLayer !== ''){
                mymap.addSource('eStation-source', {
                    'type': 'raster',
                    'tiles': [
                    'https://estation.jrc.ec.europa.eu/eStation2/webservices?SERVICE=WMS&&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image/png'+
                    '&LAYERS=' + thisProd[0].mapLayer +
                    '&TRANSPARENT=true&WIDTH=256&HEIGHT=256&CRS=EPSG:3857&BBOX={bbox-epsg-3857}'
                    ],
                    'tileSize': 256,
                });
                mymap.addLayer(
                    {
                    'id': 'eStation-layer',
                    'type': 'raster',
                    'source': 'eStation-source',
                    'paint': {}
                    }, 'wdpaAcpSelected'
                );

                var legendURL = 'https://estation.jrc.ec.europa.eu/eStation2/mobile-app/legend-html?product_id=' + thisProd[0].mapLayer
                $.ajax({
                    url: legendURL,
                    dataType: 'json',
                    success: function(d) {
                        //console.log(d.html.legendHTML)
                        $("#estation-legend").empty().append( d.html.legendHTML );
                        $("#estation-legend table").addClass("w-100");
                    },
                    error: function() {
                      console.log("Something is wrong with the REST servce for the map Legend")
                    }
                });
            } else {
                var message = '<div class="alert alert-info" role="alert">'+
                    'no map layer'+
                '</div>';
                $("#estation-legend").empty().append( message );
            }
        }
    });
    $('.estation-data-card').on('hide.bs.collapse', function () {
        var selectedTopic = $(this).closest( ".card-body" ).find(".inner-card-title").text().trim();
        if (mymap.getLayer("eStation-layer")) {
            mymap.removeLayer("eStation-layer");
        }
        if (mymap.getSource("eStation-source")) {
            mymap.removeSource("eStation-source");
        }
        if ($('#estation-'+selectedTopic+'-data-source').length > 0){
            $('#estation-'+selectedTopic+'-data-source').remove();
        }
    });

})(jQuery, Drupal);